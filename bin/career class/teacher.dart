import '../mixin class/pension.dart';
import '../mixin class/reading.dart';
import '../mother class/career.dart';

abstract class Teacher extends Career with Reading, Pension {
  Teacher(super.career);
  void teaching();
}
