import '../career class/programmer.dart';
import '../interface/salaryCalculation.dart';

class WebDeveloper extends Programmer implements SalaryCalculation {
  String? nickname;
  int? salary;
  String? position;

  WebDeveloper(String nickname, int salary, String position, super.career) {
    super.career = "web developer";
    this.nickname = nickname;
    this.position = position;
    this.salary = salary;
  }
  void introduce() {
    print(
        "สวัสดีครับ ผมชื่อ $nickname ผมทำอาชีพเป็น $career ตำแหน่งของผมคือ $position");
  }

  void getSalary() {
    print(salary);
  }

  @override
  void calProgressSalary() {
    print(
        "ความก้าวหน้าของเงินเดือน: ตามประสบการณ์การทำงานหรือตามเงื่อนไขของบริษัทนั้นๆ");
  }

  @override
  void coding() {
    print("ฉันกำลังเขียนโค้ต");
  }
}
