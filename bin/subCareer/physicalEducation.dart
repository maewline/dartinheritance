import '../career class/teacher.dart';
import '../interface/salaryCalculation.dart';

class PhysicalEducation extends Teacher implements SalaryCalculation {
  String? nickname;
  int? salary;
  String? position;

  PhysicalEducation(
      String nickname, int salary, String position, super.career) {
    super.career = "Physical Education";
    this.nickname = nickname;
    this.position = position;
    this.salary = salary;
  }

  void introduce() {
    print(
        "สวัสดีครับ ผมชื่อ $nickname ผมทำอาชีพเป็น $career ตำแหน่งของผมคือ $position");
  }

  @override
  void calProgressSalary() {
    print("คำนวณเงินเดือน: เงินเดือน * เปอร์เซ็นฐานเงินของตำแหน่งครู");
  }

  @override
  void teaching() {
    print("ฉันกำลังสอนนักเรียน");
  }
}
