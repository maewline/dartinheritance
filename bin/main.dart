import 'subCareer/physicalEducation.dart';
import 'subCareer/webDeveloper.dart';

void main(List<String> arguments) {
  //Test Web Developer
  var web = WebDeveloper("maewline", 15000, "senior", "Web developer");
  web.introduce();
  web.calProgressSalary();
  web.coding();
  web.desiging();
  web.reading();

  //Test Physical Education
  var teacher = PhysicalEducation("Arthit", 20000, "ครูชำนาญการ", "Teacher");
  teacher.introduce();
  teacher.calProgressSalary();
  teacher.teaching();
  teacher.reading();
  teacher.pension();
}
